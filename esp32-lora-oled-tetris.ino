#include <Wire.h>
#include <tetris.h>
#define WII_EXT_I2C_DEBUG(msg)  Serial.println(msg)
#include <wiiexti2c.h>
#include <vint.h>

// OLED
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
//OLED pins to ESP32 GPIOs via this connecthin:
//OLED_SDA -- GPIO4
//OLED_SCL -- GPIO15
//OLED_RST -- GPIO16
SSD1306  display(0x3c, 4, 15);

// Tetris
tetris::Game tetris_game;

// Wii
WiiExti2c controller;
WiiExtClassic classic;

// Virtual interrupts
vint::Interrupt interrupts;

// Need to tell the OLED when a draw needs to happen
bool draw_needed = false;

void setup() {

  // Serial
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Serial ready");

  // OLED
  pinMode(16,OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high
  display.init();
  // display.flipScreenVertically();
  display.setContrast(255);

  // Virtual interrupts
  interrupts.set_get_tick_count([]() -> uint32_t {
    return (uint32_t) millis();  // only acurate to 10ms
  });

  // Screen
  uint32_t screen_width = 128;
  uint32_t screen_height = 64;

  // Tetris is a 10 x 20 grid
  // 10 x 6 = 60 pixels
  // 20 x 6 = 120 pixels
  // Also need to swap x and y (use the full length of the screen)

  // Tetris graphics
  tetris_game.set_draw_clear([]() {
    display.clear();
    draw_needed = true;
  });
  tetris_game.set_draw_current_piece([](int16_t x, int16_t y, tetris::PieceType piece) {
    int16_t temp = x;
    x = y;
    y = temp;
    x = 1 + (x * 6);
    y = 1 + (y * 6);
    display.setColor(WHITE);
    x = 128 - 1 - x - 6 - 5;
    display.drawRect(x, y, 6, 6);
  });
  tetris_game.set_draw_block([](int16_t x, int16_t y) {
    int16_t temp = x;
    x = y;
    y = temp;
    x = 1 + (x * 6);
    y = 1 + (y * 6);
    display.setColor(WHITE);
    x = 128 - 1 - x - 6 - 5;
    display.fillRect(x, y, 6, 6);
  });

  // Tetris random
  tetris_game.set_random([](int16_t min, int16_t max) -> int16_t {
    return min + (esp_random() % (max + 1));
  });

  // Wii Extension
  controller.set_write([](uint8_t addr, const uint8_t *data, uint8_t size) {
    Wire.setClock(100000);  // ESP32 i2c comms glitch if any higher
    Wire.beginTransmission(addr);
    for (uint8_t i = 0; i < size; i++) {
      Wire.write(data[i]);
    }
    uint8_t error = Wire.endTransmission();
    return error == 0;
  });
  controller.set_read([](uint8_t addr, uint8_t *data, uint8_t size) {
    Wire.setClock(100000);  // ESP32 i2c comms glitch if any higher
    uint8_t recd = Wire.readBytes(data, Wire.requestFrom(addr, size));
    return recd == size;
  });
  controller.set_delay_ms([](uint32_t ms) {
    delay(ms);
  });
  controller.set_data_callback([](WiiExti2c::WiiExtType type, const uint8_t *data, uint8_t size) {
    if (type == WiiExti2c::WiiExtType::kWiiExtClassic) {
      classic.update(data, size);
    }
  });

  // Wii Classic controller buttons for Tetris
  classic.set_button_down([](WiiExtButton button) {
    switch(button) {
      case kWiiExtButtonDpadLeft:
        tetris_game.key_left();
        break;
      case kWiiExtButtonDpadRight:
        tetris_game.key_right();
        break;
      case kWiiExtButtonDpadDown:
        tetris_game.key_down();
        break;
      case kWiiExtButtonB:
        tetris_game.key_rotate();
        break;
      case kWiiExtButtonA:
        tetris_game.key_drop();
        break;
      case kWiiExtButtonPlusStart:
        tetris_game.reset();
        break;
    }
  });

  // Tetris frame rate
  interrupts.set_interrupt(700, [](){
    tetris_game.tick();
    draw_needed = true;
  });

  // Snake reset
  tetris_game.reset();

  // When game over, reset the game
  tetris_game.set_game_over([](){
    tetris_game.reset();
  });
}

void loop() {
  interrupts.run_loop();
  controller.run_loop();
  if (draw_needed) {
    draw_needed = false;
    display.setColor(WHITE);
    display.drawRect(0, 0, 122, 62);
    display.display();
  }
}

